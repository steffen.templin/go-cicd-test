package main

import (
	"fmt"
)

func main() {
	fizzbuzzer := FizzBuzzer{}
	for i := 1; i <= 100; i++ {
		fmt.Println(fizzbuzzer.GetLine(i))
	}
}

type FizzBuzzer struct{}

func (f *FizzBuzzer) GetLine(i int) string {
	if i%15 == 0 {
		return "Fizz Buzz"
	} else if i%3 == 0 {
		return "Fizz"
	} else if i%5 == 0 {
		return "Buzz"
	} else {
		return fmt.Sprint(i)
	}
}
