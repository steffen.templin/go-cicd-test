package main

import (
	"testing"
)

func TestFizzBuzz(t *testing.T) {
	fizzbuzzer := FizzBuzzer{}

	fizzes := []int{3, 6, 9, 33, 99}

	for _, i := range fizzes {
		if line := fizzbuzzer.GetLine(i); line != "Fizz" {
			t.Errorf("Expected Fizz for id=%d but got %s", i, line)
		}
	}

	buzzes := []int{5, 10, 20, 25, 50, 100}
	for _, i := range buzzes {
		if line := fizzbuzzer.GetLine(i); line != "Buzz" {
			t.Errorf("Expected Buzz for id=%d but got %s", i, line)
		}
	}

	fizzbuzzes := []int{15, 30, 45, 60, 75, 90}
	for _, i := range fizzbuzzes {
		if line := fizzbuzzer.GetLine(i); line != "Fizz Buzz" {
			t.Errorf("Expected Fizz Buzz for id=%d but got %s", i, line)
		}
	}

}
